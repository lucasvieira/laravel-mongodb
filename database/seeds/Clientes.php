<?php

use App\Models\Cliente;
use Illuminate\Database\Seeder;

class Clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cliente = new Cliente();
        $cliente->nome = 'Lucas Vieira';
        $cliente->cpf = '12345678901';
        $cliente->nascimento = '1996-02-09';
        $cliente->nomedamae = 'Solange Vieira';

        $cliente->enderecos = [
            [
                'cep' => '65058-208',
                'rua' => 'Rua Caxias',
                'numero' => 23,
                'bairro' => 'Cidade Operária',
                'complemento' => 'Village dos Mestres',
                'cidade' => 'São Luís',
                'estado' => 'Maranhão'
            ]
        ];

        $cliente->telefones = [
            [
                'ddd' => '98',
                'numero' => '985008716',
                'tipo' => 'pessoal'
            ]
        ];

        $cliente->save();
    }
}
