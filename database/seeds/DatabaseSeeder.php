<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(Users::class);
         $this->call(Clientes::class);
         $this->call(Empresas::class);
    }
}
