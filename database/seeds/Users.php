<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apiUser = new User();
        $apiUser->name = 'API User';
        $apiUser->email = 'api@user.com';
        $apiUser->password = bcrypt("123456");
        $apiUser->save();
    }
}
