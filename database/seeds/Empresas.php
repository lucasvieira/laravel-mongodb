<?php

use App\Models\Empresa;
use Illuminate\Database\Seeder;

class Empresas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa = new Empresa();
        $empresa->razaosocial = 'Fantasy Inc.';
        $empresa->inscricaoestadual = '123456789010233';
        $empresa->cnpj = '12345678/156677';
        $empresa->site = 'www.fantasyinc.com';
        $empresa->site = 'fantasy@fantasy.com';

        $empresa->enderecos = [
            [
                'cep' => '65050-200',
                'rua' => 'Av. Daniel de La Touche',
                'numero' => 123,
                'bairro' => 'Turu',
                'complemento' => '',
                'cidade' => 'São Luís',
                'estado' => 'Maranhão'
            ],
            [
                'cep' => '65041-156',
                'rua' => 'Av. Litorânea',
                'numero' => 156,
                'bairro' => 'Ponta Da Areia',
                'complemento' => 'Espigão Costeiro',
                'cidade' => 'São Luís',
                'estado' => 'Maranhão'
            ]
        ];

        $empresa->telefones = [
            [
                'ddd' => '98',
                'numero' => '33026071',
                'tipo' => 'comercial'
            ],
            [
                'ddd' => '98',
                'numero' => '33019988',
                'tipo' => 'comercial'
            ]
        ];

        $empresa->save();
    }
}
