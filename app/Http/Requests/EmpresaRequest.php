<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST') {
            return [
                'razaosocial' => 'required|string|max:50',
                'inscricaoestadual' => 'required|alpha_num|max:15',
                'cnpj' => 'required|alpha_num|min:14|max:14',
                'site' => 'required|url',
                'email' => 'required|email',

                'enderecos' => 'array|nullable',
                'enderecos.*.cep' => 'required|alpha_num',
                'enderecos.*.logradouro' => 'required|string|max:40',
                'enderecos.*.numero' => 'required|numeric',
                'enderecos.*.bairro' => 'required|string|max:40',
                'enderecos.*.complemento' => 'required|string|max:40',
                'enderecos.*.cidade' => 'required|string|max:40',
                'enderecos.*.estado' => 'required|string|min:2|max:20',

                'telefones' => 'array|nullable',
                'telefones.*.ddd' => 'required|alpha_num|max:3',
                'telefones.*.numero' => 'required|alpha_num|min:8|max:15',
                'telefones.*.tipo' => 'required|in:residencial,comercial,pessoal',
            ];
        }

        return [
            'razaosocial' => 'string|max:50',
            'cnpj' => 'alpha_num|min:14|max:14',
            'site' => 'url',
            'email' => 'email',

            'enderecos' => 'array|nullable',
            'enderecos.*.cep' => 'required|alpha_num',
            'enderecos.*.rua' => 'required|string|max:40',
            'enderecos.*.logradouro' => 'required|numeric',
            'enderecos.*.bairro' => 'required|string|max:40',
            'enderecos.*.complemento' => 'required|string|max:40',
            'enderecos.*.cidade' => 'required|string|max:40',
            'enderecos.*.estado' => 'required|string|min:2|max:20',

            'telefones' => 'array|nullable',
            'telefones.*.ddd' => 'required|alpha_num|max:3',
            'telefones.*.numero' => 'required|alpha_num|min:8|max:15',
            'telefones.*.tipo' => 'required|in:residencial,comercial,pessoal',
        ];
    }
}
