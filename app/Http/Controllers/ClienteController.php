<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ClienteRequest;

class ClienteController extends Controller
{
    public function index() {
        try {
            $data = Cliente::all();
            return new JsonResponse($data, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(ClienteRequest $request)
    {
        try {
            $cliente = new Cliente();
            $cliente->fill($request->all());
            $cliente->save();

            return new JsonResponse($cliente, JsonResponse::HTTP_CREATED);

        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(Cliente $cliente) {
        try {
            return new JsonResponse($cliente, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(ClienteRequest $request, Cliente $cliente)
    {
        try {
            $cliente->fill($request->all());
            $cliente->save();
            return new JsonResponse($cliente, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(Cliente $cliente)
    {
        try {
            $cliente->delete();
            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
