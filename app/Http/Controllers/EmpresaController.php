<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\EmpresaRequest;

class EmpresaController extends Controller
{
    public function index() {
        try {
            $data = Empresa::all();
            return new JsonResponse($data, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(EmpresaRequest $request)
    {
        try {
            $empresa = new Empresa();
            $empresa->fill($request->all());
            $empresa->save();

            return new JsonResponse($empresa, JsonResponse::HTTP_CREATED);

        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(Empresa $empresa) {
        try {
            return new JsonResponse($empresa, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function connect(Empresa $empresa, Cliente $cliente)
    {
        try {
            $associacoes = $empresa->clientes ? $empresa->clientes : [];
            $associacoes[] = $cliente->getKey();
            $empresa->clientes = array_unique($associacoes);
            $empresa->save();
            return new JsonResponse($empresa, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(EmpresaRequest $request, Empresa $empresa)
    {
        try {
            $empresa->fill($request->all());
            $empresa->save();
            return new JsonResponse($empresa, JsonResponse::HTTP_OK);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(Empresa $empresa)
    {
        try {
            $empresa->delete();
            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $exception)  {
            return new JsonResponse([
                'error' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
