<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Empresa extends Model
{
    protected $collection = 'empresas';

    protected $fillable = [
        'razaosocial',
        'inscricaoestadual',
        'cnpj',
        'site',
        'email',
        'nomedamae',
        'enderecos',
        'telefones',
    ];
}
