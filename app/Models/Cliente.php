<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Cliente extends Model
{
    protected $collection = 'clientes';

    protected $fillable = [
        'nome',
        'cpf',
        'nascimento',
        'email',
        'nomedamae',
        'enderecos',
        'telefones',
    ];
}
