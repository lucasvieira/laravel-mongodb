# Excluir cadastro de empresa

Excluir o cadastro do empresa.

**URL** : `/api/v1/empresa/:id/`

**Parâmetros da URL** : `id=[alpha_num]` Id do documento do empresa no banco de dados.

**Method** : `DELETE`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : O documento existe e foi excluido.

**Status** : `204 NO CONTENT`

**Conteúdo** : `{}`

## Respostas de erro

**Condição** : Não existe registro do empresa.

**Status** : `404 NOT FOUND`

**Conteúdo** : 
```json
{
    "message": "No query results for empresa"
}
```