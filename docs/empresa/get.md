# Exibir lista de empresas

Mostra os empresas cadastrados.

**URL** : `/api/v1/empresas/`

**Method** : `GET`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : Existem cadastro de empresas no banco de dados.

**Status** : `200 OK`

**Conteúdo** : 
```json
[
    {
        "_id": "id",
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "pessoal"
            }
        ],
        "updated_at": "2019-01-22 19:41:55",
        "created_at": "2019-01-22 19:41:55"
    }
]
```

# Exibir cadastro de empresa

Mostra o cadastro do empresa especificado.

**URL** : `/api/v1/empresas/:id`

**Parâmetros da URL** : `id=[alpha_num]` Id do documento do empresa no banco de dados.

**Method** : `GET`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : Existem empresa com o ID especificado.

**Status** : `200 OK`

**Conteúdo** : 
```json
[
    {
        "_id": "id",
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "pessoal"
            }
        ],
        "updated_at": "2019-01-22 19:41:55",
        "created_at": "2019-01-22 19:41:55"
    }
]
```

## Respostas de erro

**Condição** : Não existe registro do empresa.

**Status** : `404 NOT FOUND`

**Conteúdo** : 
```json
{
    "message": "No query results for empresa"
}
```