# Login

Usado para se autenticar na API
**URL** : `/api/v1/auth/login/`

**Método** : `POST`

**Autenticação requerida** : Não

**Dados obrigatórios**

```json
{
    "email": "[email do usuario]",
    "password": "[senha em texto plano]"
}
```

**Exemplo**

```json
{
    "username": "api@user.com",
    "password": "123456"
}
```

## Resposta

**Code** : `200 OK`

**Content example**

```json
{
    "access_token": "random_token",
    "token_type": "bearer",
    "expires_in": 3600
}
```

## Resposta de erro

**Condição** : Se 'email' ou 'password' estão incorretos ou ausentes.

**Status** : `401`

**Conteúdo** :

```json
{
    "error": "Unauthorized"
}
```