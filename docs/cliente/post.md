# Criar registro de cliente

Cria um novo registro de cliente

**URL** : `/api/v1/cliente/`

**Method** : `POST`

**Autenticação Requerida** : Sim

**Dados requiridos** : 
```json
[
    {
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "pessoal"
            }
        ],
    }
]
```
## Resposta

**Condição** : O registro foi criado.

**Status** : `201 CREATED`

**Conteúdo** : `{}`

# Respostas de Erro

**Condição** : Algum dos campos requiridos não é aceito pela validação

**Status** : `422 UNPROCESSABLE ENTITY`

**Conteúdo**: 
```json
{
    "message": "The given data was invalid.",
    "errors": {
        "enderecos.0.logradouro": [
            "The enderecos.0.logradouro field is required."
        ]
    }
}
```