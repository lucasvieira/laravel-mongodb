# Exibir lista de clientes

Mostra os clientes cadastrados.

**URL** : `/api/v1/clientes/`

**Method** : `GET`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : Existem cadastro de clientes no banco de dados.

**Status** : `200 OK`

**Conteúdo** : 
```json
[
    {
        "_id": "id",
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "pessoal"
            }
        ],
        "updated_at": "2019-01-22 19:41:55",
        "created_at": "2019-01-22 19:41:55"
    }
]
```

# Exibir cadastro de cliente

Mostra o cadastro do cliente especificado.

**URL** : `/api/v1/clientes/:id`

**Parâmetros da URL** : `id=[alpha_num]` Id do documento do cliente no banco de dados.

**Method** : `GET`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : Existem cliente com o ID especificado.

**Status** : `200 OK`

**Conteúdo** : 
```json
[
    {
        "_id": "id",
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "pessoal"
            }
        ],
        "updated_at": "2019-01-22 19:41:55",
        "created_at": "2019-01-22 19:41:55"
    }
]
```

## Respostas de erro

**Condição** : Não existe registro do cliente.

**Status** : `404 NOT FOUND`

**Conteúdo** : 
```json
{
    "message": "No query results for cliente"
}
```