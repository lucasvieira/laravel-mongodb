# Excluir cadastro de cliente

Excluir o cadastro do cliente.

**URL** : `/api/v1/cliente/:id/`

**Parâmetros da URL** : `id=[alpha_num]` Id do documento do cliente no banco de dados.

**Method** : `DELETE`

**Autenticação Requerida** : Sim

**Dados requiridos** : `{}`

## Resposta

**Condição** : O documento existe e foi excluido.

**Status** : `204 NO CONTENT`

**Conteúdo** : `{}`

## Respostas de erro

**Condição** : Não existe registro do cliente.

**Status** : `404 NOT FOUND`

**Conteúdo** : 
```json
{
    "message": "No query results for cliente"
}
```