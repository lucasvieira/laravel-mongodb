# Atualizar registro de cliente

Atualiza um novo registro de cliente

**URL** : `/api/v1/clientes/:id`

**Parâmetros da URL** : `id=[alpha_num]` Id do documento do cliente no banco de dados.

**Method** : `PUT`

**Autenticação Requerida** : Sim

**Dados requiridos** : 
```json
[
    {
        "nome": "nome",
        "cpf": "12345678901",
        "nascimento": "1996-02-09",
        "nomedamae": "Fulana",
        "enderecos": [
            {
                "cep": "65058-208",
                "rua": "Rua Caxias",
                "numero": 23,
                "bairro": "Cidade Operária",
                "complemento": "Village dos Mestres",
                "cidade": "São Luís",
                "estado": "Maranhão"
            }
        ],
        "telefones": [
            {
                "ddd": "98",
                "numero": "123456788",
                "tipo": "comercial"
            }
        ],
    }
]
```
## Resposta

**Condição** : O registro foi atualizado.

**Status** : `200 OK`

**Conteúdo** :
```json
{
    "_id": "5c4775aec6f22d184e24d613",
    "nome": "Fulano",
    "cpf": "12345678901",
    "nascimento": "1996-02-09",
    "email": "felipe@lucas.com",
    "nomedamae": "Fulano",
    "enderecos": [
        {
            "cep": "65058208",
            "logradouro": "Rua Caxias, 23",
            "numero": "23",
            "bairro": "Cidade Operaria",
            "complemento": "Vill Mestres",
            "cidade": "São Luís",
            "estado": "Maranhão"
        }
    ],
    "telefones": [
        {
            "ddd": "98",
            "numero": "985008716",
            "tipo": "comercial"
        }
    ],
    "updated_at": "2019-01-22 19:58:03",
    "created_at": "2019-01-22 19:57:34"
}
```

# Respostas de Erro

**Condição** : Algum dos campos requiridos não é aceito pela validação

**Status** : `422 UNPROCESSABLE ENTITY`

**Conteúdo**: 
```json
{
    "message": "The given data was invalid.",
    "errors": {
        "enderecos.0.logradouro": [
            "The enderecos.0.logradouro field is required."
        ]
    }
}
```

## Ou

**Condição** : Não existe registro do cliente.

**Status** : `404 NOT FOUND`

**Conteúdo** : 
```json
{
    "message": "No query results for cliente"
}
```