# LARAVEL MONGO -  REST API

Teste para vaga remota.

## Usando

- [Laravel 5.7](https://github.com/laravel/laravel/tree/v5.7.0)
- [JWT Auth](https://github.com/tymondesigns/jwt-auth)
- [Laravel MongoDB](https://github.com/jenssegers/laravel-mongodb)

# Ambiente de Desenvolvimento utilizado
* Fedora 29   
* Mongo Server
* PHP 7.2.14

# Configuração da aplicação

1 - Após fork do projeto, instalar as dependências, executando o seguinte comando na raiz do projeto:
```composer install```

2 - Configurar dados de acesso ao banco em seu arquivo ```.env```

3 - Executar os comandos para criar as Collections e popular a base de dados:

* ```php artisan migrate```   

* ```php artisan db:seed```

# Documentação da API

## Endpoints abertos

* [Login](docs/login.md): ```POST /api/v1/login```

## Endpoints que necessitam de autenticação

Estes endpoints necessitam de um token JWT válido a ser incluído no header das requisições.

### Cliente

Endpoints para manipulação de registros de clientes.

* [Exibir lista de clientes](docs/cliente/get.md) : `GET /api/v1/clientes/`
* [Criar registro de cliente](docs/cliente/post.md) : `POST /api/v1/clientes/`
* [Exibir registro de cliente](docs/cliente/pk/get.md) : `GET /api/v1/clientes/:id/`
* [Atualizar um registro de cliente](docs/cliente/put.md) : `PUT /api/v1/clientes/:id/`
* [Excluir um registro de cliente](docs/cliente/delete.md) : `DELETE /api/v1/clientes/:id/`

### Empresa

Endpoints para manipulação de registros de empresas.

* [Exibir lista de empresas](docs/empresa/get.md) : `GET /api/v1/empresas/`
* [Criar registro de empresa](docs/empresa/post.md) : `POST /api/v1/empresas/`
* [Exibir registro de empresa](docs/empresa/pk/get.md) : `GET /api/v1/empresas/:id/`
* [Atualizar um registro de empresa](docs/empresa/put.md) : `PUT /api/v1/empresas/:id/cliente/:id`
* [Associar um cliente à empresa](docs/empresa/put.md) : `PUT /api/v1/empresas/:id/`
* [Excluir um registro de empresa](docs/empresa/delete.md) : `DELETE /api/v1/empresas/:id/`