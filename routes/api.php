<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', function () {
    return "Hello, API";
});

Route::group(['middleware' => ['api'], 'prefix' => 'v1'], function () {

    // JWT Auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });

    Route::group(['prefix' => 'clientes', 'middleware' => 'jwt.verify'], function () {
        Route::get('/', 'ClienteController@index')->name('clientes.index');
        Route::post('/', 'ClienteController@store')->name('clientes.store');
        Route::get('/{cliente}', 'ClienteController@show')->name('clientes.show');
        Route::put('/{cliente}', 'ClienteController@update')->name('clientes.update');
        Route::delete('/{cliente}', 'ClienteController@destroy')->name('clientes.destroy');
    });

    Route::group(['prefix' => 'empresas', 'middleware' => 'jwt.verify'], function () {
        Route::get('/', 'EmpresaController@index')->name('empresas.index');
        Route::post('/', 'EmpresaController@store')->name('empresas.store');
        Route::get('/{empresa}', 'EmpresaController@show')->name('empresas.show');
        Route::put('/{empresa}', 'EmpresaController@update')->name('empresas.update');
        Route::put('/{empresa}/cliente/{cliente}', 'EmpresaController@connect')->name('empresas.connect');
        Route::delete('/{empresa}', 'EmpresaController@destroy')->name('empresas.destroy');
    });
});